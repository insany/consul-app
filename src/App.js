import React from 'react';
import {Router, Route} from "react-router-dom";
import { createBrowserHistory } from "history";
import Cadastro from "./routes/cadastro";

import './css/grid.css';
import Home from './views/home';
import Atendimento from './routes/atendimento/index';
import ScrollToTop from './ScrollToTop'



function App() {
  const hist = createBrowserHistory();

  return (
    <div className="App main">
      <Router history={hist}>
        <ScrollToTop>
          <Route path="/" exact component={Home} />
          <Route path="/cadastro" component={Cadastro} />
          <Route path="/atendimento" component={Atendimento} />
        </ScrollToTop>
      </Router>
    </div>
  );
}

export default App;
