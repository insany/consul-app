import React, { Component } from 'react'
import styled from 'styled-components'
import MainButton from './button';

import {ReactComponent as Next} from '../img/caret-right-circle.svg'
import {ReactComponent as Edit} from '../img/edit.svg'


const IconContainer = styled.div `
  border-radius: 22px;
  border: 2px solid ${(props) => props.att ? "#b1df50": "#c4c4c4"};
  position: relative;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  display: flex;

  svg{
    width: 15px;
    height: 15px;
    margin: 13px;
  }

  g{
    opacity: 1;
  }
`

const LeftIconContainer = styled.div `
  display: flex;
  align-self: flex-start;
  cursor: pointer;

  svg{
    width: 15px;
    height: 15px;
  }

  g{
    opacity: 1;
  }
`

const InfoContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 0px 25px;

  & h1{
    font-size: 16px;
    font-weight: 400;
    line-height: 20px;
    margin-bottom: 6px;
  }

  & p{
    color: #a1a1a1;
    font-size: 12px;
    font-weight: 300;
    line-height: 15px;
    margin-bottom: 21px;
  }

  & li{
    list-style-type: initial;
    list-style-position: inside;
    list-style-image: initial;
    color: #a1a1a1;
    font-size: 12px;
    font-weight: 300;
    line-height: 20px;
  }

  & table{
    width: 120%;
  }

  & tr{
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    border-bottom: 1px solid #E6E6E6;
  }

  & .total{
    width: 120%;
    margin-top: 16px;
    display: flex;
    justify-content: space-between;
    align-items: center;

    & h3{
      color: #a1a1a1;
      font-size: 16px;
      font-weight: 400;
      line-height: 20px;
      text-transform: uppercase;
    }

    & span{
      color: #a1a1a1;
      font-size: 16px;
      font-weight: 700;
      line-height: 20px;
      text-transform: uppercase;
    }
  }

  & .cell-nome-servico{
    color: #a1a1a1;
    font-size: 12px;
    font-weight: 300;
    line-height: 25px;
  }

  & .cell-preco-servico{
    color: #a1a1a1;
    font-size: 12px;
    font-weight: 500;
    line-height: 25px;
    text-transform: uppercase;
  }

  & .pagamento-card {
    width: 240px;
    height: 152px;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
    border-radius: 8px;
    background-color: #ffffff;
    padding: 14px 0;

    & .card-header{
      padding: 0 21px;
      color: #a1a1a1;
      line-height: 20px;
      font-size: 16px;
      font-weight: 500;
      border-bottom: 1px solid #E6E6E6;
    }

    & .card-info{
      & ul{
        margin-top: 25px;
        padding: 0 21px;
      }

      & li{
        list-style: none;
        color: #a1a1a1;
        font-size: 16px;
        font-weight: 500;
        line-height: 20px;

        & span{
          font-size: 16px;
          font-weight: 300;
        }
      }
    }
  }
`

const AtendimentoSectionItem = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  padding: 30px;
  border-bottom: 1px solid #E6E6E6;

  & h1{
    color: ${(props) => props.att ? "#b1df50": "#c4c4c4"};
  }
  & h1{
    color: ${(props) => props.att ? "#b1df50": "#c4c4c4"};
  }

  & path {
    stroke: ${(props) => props.att ? "#b1df50": "#c4c4c4"};
  }


`

class HasBtn extends Component {
  render() {
    if (this.props.hasBtn) {
      return (
        <MainButton to={this.props.btnTo} btnText={this.props.btnText} />
      );
    } else {
      return null;
    }
  }
}

class HasLeftBtn extends Component {
  render() {
    if (this.props.leftBtn == 'next') {
      return (
        <LeftIconContainer >
          <Next />
        </LeftIconContainer>
      );
    } else if (this.props.leftBtn == 'edit') {
      return (
        <LeftIconContainer >
          <Edit />
        </LeftIconContainer>
      );
    } else {
      return null;
    }
  }
}


export default class AtendimentoStep extends Component {
  render() {
    return (
      <AtendimentoSectionItem att={this.props.att}>
        <IconContainer att={this.props.att} className="icon-container-atendimento">
          {this.props.children[0]}
        </IconContainer>
        <div className="div-step">
          <InfoContainer>
            <h1>{this.props.n}.&nbsp;{this.props.title}</h1>
            {this.props.children[1]}
            {this.props.children[2] ? this.props.children[2] : null}
            <HasBtn hasBtn={this.props.hasBtn} btnText={this.props.btnText} btnTo={this.props.btnTo} />
          </InfoContainer>
        </div>
        <HasLeftBtn leftBtn={this.props.leftBtn} />
      </AtendimentoSectionItem>
    )
  }
}
