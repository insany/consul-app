import React, { Component } from 'react'
import './css/atendimentoTabs.css'
import styled from 'styled-components';


const Tab = styled.div`
  cursor: pointer;
  width: 100%;
  height: 62px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #a1a1a1;
  font-size: 12px;
  font-weight: 500;
  line-height: 19px;
  text-transform: uppercase;

  ${props => props.active ? 'border-bottom: 3px solid #b1df50' : 'border-bottom: none'}
`


export default class AtendimentoTabs extends Component {
    constructor(props) {
      super(props);
      this.state = {
        tab1: true,
        tab2: false
      }
  }



  render() {
    return (
      <div className="atendimento-tabs-container">
        <Tab
          active={this.state.tab1}
          onClick={() => {
            this.setState({tab1:true, tab2:false});
            this.props.handleTabChange(1)
          }}
        >
          Ativos
        </Tab>
        <Tab
          active={this.state.tab2}
          onClick={() => {
            this.setState({tab2:true, tab1:false});
            this.props.handleTabChange(2)
          }}
        >
          Finalizados
        </Tab>
      </div>
    )
  }
}
