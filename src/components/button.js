import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom';

const BtnWrapper = styled.div`
  width: 100%;

  &:first-child{
    width: 100%
  }
`

const Button = styled.div `
  cursor: pointer;
  width: 100%;
  height: 57px;
  box-shadow: 0 4px 11px 1px rgba(196, 196, 196, 0.42);
  background-color: #b1df50;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 40px;
  font-size: 14px;
  font-weight: 700;
  color: #ffffff;
  text-transform: uppercase;
`

export default class MainButton extends Component {
  render() {
    return (
      <BtnWrapper>
        <Link
          to={{
            pathname: this.props.to,
            state:{
              from: this.props.from
            }
          }}
          className='main-btn'
        >
          <Button>
            {this.props.btnText}
          </Button>
        </Link>
      </BtnWrapper>
    )
  }
}
