import React, { Component } from 'react'
import TextInput from './textInput';

export default class CursosContainer extends Component {
  render() {
    return (
      <div className="cursos-container">
        <TextInput label="Nome do Curso" type="text" />
        <TextInput label="Instituição" type="text" />
        <TextInput label="Duração" type="text" />
      </div>
    )
  }
}
