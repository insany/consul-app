import React, {Component} from 'react';
import styled from "styled-components";
import {withRouter} from 'react-router-dom';

import MenuButton from './menu-button';

import Logo from '../img/logo.png';
import Avatar from '../img/avatar.svg'

import '../components/css/header.css';

const Header = styled.header`
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    padding-left: 21px;
    padding-right: 18px;
    background-color: red;
    width: 100%;
    height: 58px;
    box-shadow: 0 3px 4px rgba(73, 106, 0, 0.2);
    background-color: #ffffff;
    display: flex;
    align-items: center;
    justify-content: space-between;
    z-index: 99;
`

const MenuText = styled.div`
  color: #a1a1a1;
  font-size: 14px;
  font-weight: 500;
  line-height: 19px;
`

const StepsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const Step = styled.div `
  width: 7px;
  height: 7px;
  margin-left: 10px;
  border-radius: 7px;
  background-color: ${(props) => props.active ? "#b1df50": "#c4c4c4"};
`

class LogoHeader extends Component {
  render() {
    if(this.props.hasLogo){
      return (
        <img src={Logo}/>
      );
    }else {
      return null;
    }
  }
}

class TextHeader extends Component {
  render() {
    if (this.props.hasText) {
      return (
        <MenuText>{this.props.text}</MenuText>
      );
    } else {
      return null;
    }
  }
}

class AvatarHeader extends Component {
  render() {
    if (this.props.hasAvatar) {
      return (
        <div className="avatar">
          <img src={Avatar}/>
        </div>
      );
    } else {
      return null;
    }
  }
}

class StepsHeader extends Component {
  render() {
    const nSteps = this.props.steps
    const steps = [];
    for (let i = 1; i <= nSteps; i++) {
      steps.push(i);
    }
    let active = this.props.active;
    const Steps = steps.map((step) => <Step key={step} active={false} />);
    Steps[active-1]=<Step key={active} active />
    if (this.props.hasSteps) {
      return (
        <StepsContainer>
          {Steps}
        </StepsContainer>
      )
    } else {
      return null;
    }
  }
}

class HeaderComponent extends Component {
    constructor(props) {
      super(props);

      this.state = {};
    }


    render() {
        return (
            <Header>
                <MenuButton type={this.props.menuType}/>

                <LogoHeader hasLogo={this.props.hasLogo} />

                <TextHeader hasText={this.props.hasText} text={this.props.text} />

                <StepsHeader hasSteps={this.props.hasSteps} steps={this.props.steps} active={this.props.active} />

                <AvatarHeader hasAvatar={this.props.hasAvatar} />
            </Header>
        );
    }
}

export default withRouter(HeaderComponent);
