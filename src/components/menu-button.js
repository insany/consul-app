import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import '../components/css/menu-button.css';

import goBack from '../img/goBack.svg'

class MenuButton extends Component {
    ativarBotao = (e) => {
        e.preventDefault();
        document.documentElement.classList.toggle('menu-opened');
    }

    render() {
      if(this.props.type == 'menu'){
        return (
            <div>
                <a className="menu-button" onClick={this.ativarBotao}>
                    <i className="menu-icon"></i>
                </a>
            </div>
        );
      }else if (this.props.type == 'goBack') {
        return (
            <div>
                <a className="menu-button" onClick={() => this.props.history.goBack()}>
                  <img src={goBack} />
                </a>
            </div>
        );
      }

    }
}

export default withRouter(MenuButton);

