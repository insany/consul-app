import React, { Component } from 'react'

import mail from '../img/mail.svg';
import phone from '../img/phone.svg';
import photo from '../img/avatar.png'


export default class Perfil extends Component {
  render() {
    return (
      <div className="perfil-info s-info">
        <div className="foto">
          <img src={photo} />
        </div>
        <div className="perfil-info-data">
          <h3 className="perfil-nome">
            Josias Nascimento
          </h3>
          <div className="perfil-email-container">
            <img src={mail} />
            <span className="data-text">
              j.nascimento@gmail.com
            </span>
          </div>
          <div className="perfil-phone-container">
            <img src={phone} />
            <span className="data-text">
              (11) 97867-0989
            </span>
          </div>
        </div>
      </div>
    )
  }
}
