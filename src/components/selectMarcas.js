/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'

import styled, {css} from 'styled-components';

const MarcaContainer = styled.div`
  cursor: pointer;
  width: 100%;
  height: 45px;
  background-color: ${(props) => props.active ? "#b1df50": "white"};
  box-shadow: ${(props) => props.active ? "0 2px 4px rgba(0, 0, 0, 0.12)" : "none"  };
  border-radius: 23px;
  border: 1px solid #b1df50;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${(props) => props.active ? "white": "#8dce00"};
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  line-height: 20px;
  text-align: center;
  flex: 1 0 21%;


  ::selection {
      background: none;
  }
`

export default class SelectMarca extends Component {
    constructor(props) {
      super(props);

      this.handleClick = this.handleClick.bind(this);

      this.state = {
        isSelected: false
      };
    }

  handleClick() {
    const newBoolVarStatus = !this.state.isSelected;
    this.setState({
      isSelected: newBoolVarStatus
    });
  }


  render() {
    return (
        <MarcaContainer onClick={this.handleClick} active={this.state.isSelected}>
            {this.props.nomeMarca}
        </MarcaContainer>
    )
  }
}
