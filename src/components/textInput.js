/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import {withRouter} from 'react-router-dom';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

import './css/inputs.css';

class TextInput extends Component {


    render() {

        return (
          <FormControl className="form-group">
            <InputLabel shrink={this.props.shrink}>{this.props.label}</InputLabel>
            <Input type={this.props.type} value={this.props.value} autoFocus	/>
          </FormControl>
        );
    }
}

export default withRouter(TextInput);
