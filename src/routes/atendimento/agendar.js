import React from 'react'
import {Route, Redirect} from "react-router-dom"

import agendarData from '../../views/atendimento/agendar/agendarData';
import agendarHora from '../../views/atendimento/agendar/agendarHora';

export default function Agendar() {
  return (
    <div>
      <Route path="/atendimento/agendar/data" exact component={agendarData} />
      <Route path="/atendimento/agendar/hora" exact component={agendarHora} />
      <Route path="/atendimento/agendar" render={()=>(
        <Redirect strict from='/atendimento/agendar' to='/atendimento/agendar/data' />
      )}/>
    </div>
  )
}
