import React from 'react'
import {Route, Redirect} from "react-router-dom";

import ClienteAtendimento from './../../views/atendimento/clienteAtendimento';
import ProdutoAtendimento from './../../views/atendimento/produtoAtendimento';
import atendimento from '../../views/atendimento/atendimento.js';
import Agendar from './agendar';
import atendimentoCompartilha from '../../views/atendimento/atendimentoCompartilha';


export default function Atendimento() {
  return (
    <div>
      <Route path="/atendimento" exact component={atendimento} />
      <Route path="/atendimento/criar" render={()=>(
        <Redirect strict from='/atendimento/criar' to='/atendimento/cliente' />
      )}/>
      <Route path="/atendimento/cliente"component={ClienteAtendimento} />
      <Route path="/atendimento/produto" component={ProdutoAtendimento} />
      <Route path="/atendimento/agendar" component={Agendar} />
      <Route path="/atendimento/compartilhar" component={atendimentoCompartilha} />
      <Route path="/atendimento/projetocarol" component={atendimentoCompartilha} />
    </div>
  )
}
