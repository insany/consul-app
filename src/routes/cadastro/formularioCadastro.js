import React from 'react'
import {Route,Switch} from 'react-router-dom'

import FormularioExperiencia from './formularioExperiencia';
import PerfilCadastro from './../../views/cadastro/perfilCadastro';



export default function FormularioCadastro() {
  return (
    <div>
      <Route path="/cadastro/formulario" exact component={PerfilCadastro} />
      <Route path="/cadastro/formulario/experiencia" component={FormularioExperiencia} />
    </div>
  )
}
