import React from 'react'
import {Route} from 'react-router-dom';
import ExperienciaMarcas from '../../views/cadastro/experienciaMarcas';
import ExperienciaCursos from '../../views/cadastro/experienciaCursos';

export default function FormularioExperiencia() {
  return (
    <div>
      <Route path="/cadastro/formulario/experiencia" exact component={ExperienciaMarcas} />
      <Route path="/cadastro/formulario/experiencia/cursos" exact component={ExperienciaCursos} />
    </div>
  )
}
