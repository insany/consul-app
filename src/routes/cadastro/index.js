import React from 'react'
import {Route} from "react-router-dom";

import FormularioCadastro from './formularioCadastro';
import ConluiCadastro from '../../views/cadastro/concluiCadastro';
import InicioCadastro from "../../views/cadastro/inicioCadastro";

import '../../css/default.css';

import '../../css/cadastro.css';

export default function Cadastro() {
  return (
    <div>
      <Route path="/cadastro" exact component={InicioCadastro} />
      <Route path="/cadastro/formulario" component={FormularioCadastro} />
      <Route path="/cadastro/sucesso" component={ConluiCadastro} />
    </div>
  )
}
