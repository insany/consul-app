/* eslint-disable react/prefer-stateless-function */
import React, { Component, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import styled from 'styled-components';

import HeaderComponent from './../../../components/header';

import '../../../css/agendar.css'
import MainButton from '../../../components/button';

import "react-datepicker/dist/react-datepicker.css";

import DatePicker, {registerLocale} from "react-datepicker";
import ptBR from 'date-fns/locale/pt-BR';

registerLocale('pt-br', ptBR)


const CalendarContainer = styled.div`
  width: 100%;
`


class AgendarData extends Component {
  state = {
    startDate: new Date(),
    haveDate: false,
  };

  handleChange = date => {
    this.setState({
      startDate: date,
      haveDate: true
    });
  };

  handleDateChangeRaw = (e) => {
    e.preventDefault();
  }


  render() {
    if(this.state.haveDate){
      return(
        <Redirect
          to={{
            pathname: "/atendimento/agendar/hora",
            state: {
              agendamento: {
                data: this.state.startDate,
              }
            }
          }}
        />
      )
    }else {
      return (
        <div className="component__insany">
          <HeaderComponent menuType="goBack" hasLogo={false} hasText text="Agendar Horário" hasSteps={false} />
          <CalendarContainer>
            <DatePicker
              locale="pt-br"
              dateFormat='dd/MM/yyyy'
              startOpen

              className="calendar-date"

              minDate={new Date()}

              dropdownMode='scroll'
              fixedHeight={false}
              weekLabel={'D','D','D','D','D','D','D'}
              useWeekdaysShort


              selected={this.state.startDate}
              onChange={this.handleChange}
            />
          </CalendarContainer>
        </div>
      )
    }
  }
}

export default withRouter(AgendarData);
