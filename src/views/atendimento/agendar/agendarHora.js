/* eslint-disable react/prefer-stateless-function */
import React, { Component, useState } from 'react'
import { Link, Redirect } from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import styled from 'styled-components';
import { roundToNearestMinutes } from 'date-fns'


import HeaderComponent from '../../../components/header';

import '../../../css/agendar.css'
import MainButton from '../../../components/button';

import "react-datepicker/dist/react-datepicker.css";

import DatePicker, {registerLocale} from "react-datepicker";
import ptBR from 'date-fns/locale/pt-BR';

registerLocale('pt-br', ptBR)


const CalendarContainer = styled.div`
  width: 100%;
`



class AgendarHora extends Component {
  state = {
    data: this.props.location.state.agendamento.data,
    hora: null,
    haveDate: false,
  };

  componentDidMount(){
    this.setState({
      hora: roundToNearestMinutes(new Date(), {nearestTo:15})
    })
  }


  handleChange = hora => {
    this.setState({
      hora: hora,
      haveDate: true
    });
  };
  render() {


    if(this.state.haveDate){
      return(
        <Redirect
          push
          to={{
            pathname: "/atendimento",
            state: {
              from: this.props.location.pathname,
              agendamento: {
                data: this.state.data,
                hora: this.state.hora
              }
            }
          }}
        />
      )
    }else {
      return (
        <div className="component__insany">
          <HeaderComponent menuType="goBack" hasLogo={false} hasText text="Agendar Horário" hasSteps={false} />
          <CalendarContainer>
            <DatePicker
              locale="pt-br"
              startOpen

              showTimeSelect
              showTimeSelectOnly
              timeIntervals={15}
              dateFormat="h:mm aa"

              selected={this.state.hora}
              onChange={this.handleChange}
            />
          </CalendarContainer>
        </div>
      )
    }
  }
}

export default withRouter(AgendarHora);
