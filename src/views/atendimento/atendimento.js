import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import moment from 'moment'
import 'moment/locale/pt-br'

import HeaderComponent from '../../components/header';

import houseWhite from '../../img/house-white.svg'
import iconEdit from '../../img/edit.svg'

import '../../css/atendimento.css'

import produto from '../../img/produto.png';

import {ReactComponent as Calendar} from '../../img/calendar-alt.svg'
import {ReactComponent as List} from '../../img/list.svg'
import {ReactComponent as Dollar} from '../../img/dollar.svg'
import {ReactComponent as Barcode} from '../../img/barcode.svg'

import AtendimentoStep from '../../components/atendimentoStep';

moment.locale('pt-br');


const IconContainer = styled.div`
  border-radius: 49px;
  background-color: #b1df50;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 21px;

  & img{
    padding: 15px;
  }
`

const PagamentoContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  & a{
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 80%;
    height: 43px;
    border-radius: 22px;
    border: 1px solid #b1df50;
    margin-bottom: 14px;
  }

  & span{
    color: #b1df50;
    font-size: 10px;
    font-weight: 500;
    line-height: 20px;
    text-transform: uppercase;
  }

  .finalizar{
    border-radius: 0;
    border: none;
    margin-bottom: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 75px;
    background-color: #b1df50;

    & span{
      color: #ffffff;
      font-size: 14px;
      font-weight: 700;
      line-height: 20px;
      text-transform: uppercase;
    }
  }
`
class DiagnosticoInfo extends Component {
  render() {
    if (this.props.display){
      return (
        <ul>
          <li>Limpeza do filtro. </li>
          <li>Troca da placa inverter.</li>
          <li>Troca serpentina de cobre. </li>
        </ul>
      )
    }else{
      return null;
    }
  }
}

class OrcamentoInfo extends Component {
  render() {
    if (this.props.display){
      return (
        <div>
          <table>
            <tbody>
              <tr>
                <td className="cell-nome-servico">Mão de obra</td>
                <td className="cell-preco-servico">R$500.00</td>
              </tr>
              <tr>
                <td className="cell-nome-servico">Placa inverter Consul</td>
                <td className="cell-preco-servico">R$750.00</td>
              </tr>
              <tr>
                <td className="cell-nome-servico">Serpentina de Cobre 1/4''</td>
                <td className="cell-preco-servico">R$100.00</td>
              </tr>
              <tr>
                <td className="cell-nome-servico">Suporte para Condensadora</td>
                <td className="cell-preco-servico">R$30.00</td>
              </tr>
            </tbody>
          </table>
          <div className="total">
            <h3>TOTAL</h3>
            <span>R$ 1,380.00</span>
          </div>
        </div>
      )
    }else{
      return null;
    }
  }
}
class PagamentoInfo extends Component {
  render() {
    if (this.props.display){
      return (
          <PagamentoContainer>
            <a>
              <span>Gerar Boleto</span>
            </a>
            <a>
              <span>Fornecer Dados Bancários</span>
            </a>
            <a>
              <span>Receber em Dinheiro</span>
            </a>
            <a className="finalizar">
              <span>Finalizar Atendimento</span>
            </a>
          </PagamentoContainer>
      )
    }else{
      return null;
    }
  }
}


class atendimento extends Component {
  constructor(props) {
    super(props);
    this.state = {
      atendimento:{
        nome: "Carol Dias",
        endereco: "Rua Lorem Ipsum 4500 | Vila Lorem Apto 182",
        produto: {
          imagem: null,
          nome: "Ar-Condicionado",
          marca: "Consul",
          tipo: "Spli",
          modelo: "maxi",
          potencia: "9000 BTUs"
        },
        etapas: {
          agendamento: {
            data: null,
            hora: null,
            done: false,
            info: "Primeiro passo é agendar um dia e horário que funcione para você e seu cliente."
          },
          diagnostico: {
            done: false,
            info: "Passe uma descrição de que serviços precisam ser executados."
          },
          orcamento: {
            done: false,
            info: "Detalhamento do preço da mão de obra e peças a serem adquiridas. Lembre-se de avisar se a visita técnica será cobrada ou não."
          },
          pagamento: {
            done: false,
            info: "Assim que o serviço for finalizado e estiver tudo certinho. A gente te ajuda a receber o pagamento através de boleto ou transferência. Mas vc pode escolher receber em dinheiro também, é só combinar com o cliente."
          }
        }
      }
    }
  }

componentWillMount(){
      if (this.props.location.state.from === '/atendimento/agendar/hora') {
      this.setState(prevState => ({
        atendimento: {
          ...prevState.atendimento,
          etapas: {
            ...prevState.atendimento.etapas,
            agendamento: {
              ...prevState.atendimento.etapas.agendamento,
              done: true,
              data: this.props.location.state.agendamento.data,
              hora: this.props.location.state.agendamento.hora,
              info: `${moment(this.props.location.state.agendamento.data).format("dddd d [de] MMMM")} às ${moment(this.props.location.state.agendamento.hora).format("HH:mm")}`
            }
          }
        }
      }))
    }
}

  render() {

    return (
      <div className="component__insany">
        <HeaderComponent menuType="goBack" hasText text="Casa da Carol" />
        <div className="atendimento-desc-section">
          <div className="desc-info-atendimento">
            <IconContainer>
              <img src={houseWhite} />
            </IconContainer>
            <div className="desc-info-icon-container">
              <div className="desc-info-icon-text">
                <h2>{this.state.atendimento.nome}</h2>
                <img src={iconEdit} />
              </div>
              <span>{this.state.atendimento.endereco}</span>
            </div>
          </div>
          <div className="atendimento-produto-section">
            <img src={produto} />
            <span>
              {this.state.atendimento.produto.nome}&nbsp;
              {this.state.atendimento.produto.marca}&nbsp;
              {this.state.atendimento.produto.tipo}&nbsp;
              {this.state.atendimento.produto.modelo}&nbsp;
              {this.state.atendimento.produto.potencia}
            </span>
          </div>
        </div>
        <div className="atendimento-etapas-section">
          <AtendimentoStep
            att={this.state.atendimento.etapas.agendamento.done}
            n='1'
            title="Agendamento"
            hasBtn={!this.state.atendimento.etapas.agendamento.done}
            btnTo="/atendimento/agendar"
            btnText="AGENDAR VISITA"
            leftBtn={false}
          >
            <Calendar />
            <p>{this.state.atendimento.etapas.agendamento.info}</p>
          </AtendimentoStep>
          <AtendimentoStep
            att={this.state.atendimento.etapas.diagnostico.done}
            n='2'
            title="Diagnóstico de serviço"
            hasBtn={false}
            leftBtn={this.state.atendimento.etapas.diagnostico.done ? 'edit' : 'next'}
          >
            <List />
            <p>{this.state.atendimento.etapas.diagnostico.info}</p>
            <DiagnosticoInfo
              display={this.state.atendimento.etapas.diagnostico.done}
            />
          </AtendimentoStep>
          <AtendimentoStep
            att={this.state.atendimento.etapas.orcamento.done}
            n='3'
            title="Orçamento detalhado"
            hasBtn={false}
            leftBtn={this.state.atendimento.etapas.orcamento.done ? 'edit' : 'next'}
          >
            <Dollar />
            <p>{this.state.atendimento.etapas.orcamento.info}</p>
            <OrcamentoInfo
              display={this.state.atendimento.etapas.orcamento.done}
            />
          </AtendimentoStep>
          <AtendimentoStep
            att={this.state.atendimento.etapas.pagamento.done}
            n='4'
            title="Pagamento"
            hasBtn={false}
            LeftBtn={false}
          >
            <Barcode />
            <p>{this.state.atendimento.etapas.pagamento.info}</p>
          </AtendimentoStep>
          <PagamentoInfo
            display={this.state.atendimento.etapas.pagamento.done}
          />
        </div>
      </div>
    )
  }
}

export default withRouter(atendimento);
