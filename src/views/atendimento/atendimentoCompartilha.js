import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import moment from 'moment'
import 'moment/locale/pt-br'

import HeaderComponent from '../../components/header';

import houseWhite from '../../img/house-white.svg'
import iconEdit from '../../img/edit.svg'

import '../../css/atendimento.css'

import produto from '../../img/produto.png';

import {ReactComponent as Calendar} from '../../img/calendar-alt.svg'
import {ReactComponent as List} from '../../img/list.svg'
import {ReactComponent as Dollar} from '../../img/dollar.svg'
import {ReactComponent as Barcode} from '../../img/barcode.svg'
import Logo from '../../img/logo.png';


import AtendimentoStep from '../../components/atendimentoStep';
import Perfil from '../../components/perfil';

moment.locale('pt-br');


const IconContainer = styled.div`
  border-radius: 49px;
  background-color: #b1df50;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 21px;

  & img{
    padding: 15px;
  }
`



class atendimentoCompartilha extends Component {
  constructor(props) {
    super(props);
    this.state = {
      atendimento:{
        nome: "Carol Dias",
        endereco: "Rua Lorem Ipsum 4500 | Vila Lorem Apto 182",
        produto: {
          imagem: null,
          nome: "Ar-Condicionado",
          marca: "Consul",
          tipo: "Spli",
          modelo: "maxi",
          potencia: "9000 BTUs"
        },
        etapas: {
          agendamento: {
            done: true,
            info: "Segunda-feira 27 de outubro às 15:00 em Rua Lorem Ipsum 4500 | Apto 182"
          },
          diagnostico: {
            done: true,
            info: "Passe uma descrição de que serviços precisam ser executados."
          },
          orcamento: {
            done: true,
            info: "Detalhamento do preço da mão de obra e peças a serem adquiridas. Lembre-se de avisar se a visita técnica será cobrada ou não."
          },
          pagamento: {
            done: true,
            info: "Assim que o serviço for finalizado e estiver tudo certinho. A gente te ajuda a receber o pagamento através de boleto ou transferência. Mas vc pode escolher receber em dinheiro também, é só combinar com o cliente."
          }
        }
      }
    }
  }

  render() {

    return (
      <div className="component__insany">
        <div className="atendimento-desc-section atendimento-compartilha-section">
          <div className="atendimento-client-section">
            <img src={Logo} />
            <h2>Olá Carol,</h2>
            <p>O seu técnico disponibilizou este link para que você possa acompanhar o passo-a-passo da realização do seu serviço. Assim você tem visibilidade, transparência e a certeza de que o serviço será executado conforme o combinado e tudo em tempo real ;)</p>
          </div>
          <Perfil />
          <div className="atendimento-produto-section">
            <img src={produto} />
            <span>
              {this.state.atendimento.produto.nome}&nbsp;
              {this.state.atendimento.produto.marca}&nbsp;
              {this.state.atendimento.produto.tipo}&nbsp;
              {this.state.atendimento.produto.modelo}&nbsp;
              {this.state.atendimento.produto.potencia}
            </span>
          </div>
        </div>
        <div className="atendimento-etapas-section">
          <AtendimentoStep
            att={this.state.atendimento.etapas.agendamento.done}
            n='1'
            hasBtn={false}
            title="Agendamento"
          >
            <Calendar />
            <p>{this.state.atendimento.etapas.agendamento.info}</p>
          </AtendimentoStep>
          <AtendimentoStep
            att={this.state.atendimento.etapas.diagnostico.done}
            n='2'
            title="Diagnóstico de serviço"
            hasBtn={false}
          >
            <List />
            <ul>
              <li>Limpeza do filtro. </li>
              <li>Troca da placa inverter.</li>
              <li>Troca serpentina de cobre. </li>
            </ul>
          </AtendimentoStep>
          <AtendimentoStep
            att={this.state.atendimento.etapas.orcamento.done}
            n='3'
            title="Orçamento detalhado"
            hasBtn={false}
          >
            <Dollar />
            <div>
              <table>
                <tbody>
                  <tr>
                    <td className="cell-nome-servico">Mão de obra</td>
                    <td className="cell-preco-servico">R$500.00</td>
                  </tr>
                  <tr>
                    <td className="cell-nome-servico">Placa inverter Consul</td>
                    <td className="cell-preco-servico">R$750.00</td>
                  </tr>
                  <tr>
                    <td className="cell-nome-servico">Serpentina de Cobre 1/4''</td>
                    <td className="cell-preco-servico">R$100.00</td>
                  </tr>
                  <tr>
                    <td className="cell-nome-servico">Suporte para Condensadora</td>
                    <td className="cell-preco-servico">R$30.00</td>
                  </tr>
                </tbody>
              </table>
              <div className="total">
                <h3>TOTAL</h3>
                <span>R$ 1,380.00</span>
              </div>
            </div>
          </AtendimentoStep>
          <AtendimentoStep
            att={this.state.atendimento.etapas.pagamento.done}
            n='4'
            title="Pagamento"
            hasBtn={false}
            LeftBtn={false}
          >
            <Barcode />
            <div>
              <p>Dados para transferência:</p>
              <div className="pagamento-card">
                <div className="card-header">
                  Josias Nascimento
                </div>
                <div className="card-info">
                <ul>
                  <li>
                    CPF: <span>319.054.743-90</span>
                  </li>
                  <li>
                    BANCO: <span>Bradesco</span>
                  </li>
                  <li>
                    AGÊNCIA: <span>07683</span>
                  </li>
                  <li>
                    C/C: <span>02354-7</span>
                  </li>
                </ul>
                </div>
              </div>
            </div>
          </AtendimentoStep>
        </div>
      </div>
    )
  }
}

export default withRouter(atendimentoCompartilha);
