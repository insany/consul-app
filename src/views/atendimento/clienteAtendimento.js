/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import TextInput from '../../components/textInput';
import styled from 'styled-components';

import HeaderComponent from './../../components/header';

import {ReactComponent as Residencial} from '../../img/residencial.svg'
import {ReactComponent as Comercial} from '../../img/comercial.svg'

import '../../css/atendimento.css'
import MainButton from '../../components/button';

const TipoCliente = styled.div `
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  background-color: #ffffff;
  width: 100%;
  padding: 41px 0px 25px 0px;
  opacity: ${(props) => props.active ? "1": "0.4"};
  
  h1{
    text-align: center;
    width: 100%;
    color: #a1a1a1;
    font-size: 12px;
    font-weight: 500;
    line-height: 19px;
    text-transform: uppercase;
    margin-top: 16px
  }
`

class ClienteAtendimento extends Component {
    constructor(props) {
    super(props);

    this.handleClickComercial = this.handleClickComercial.bind(this);
    this.handleClickResidencial = this.handleClickResidencial.bind(this);

    this.state = {
      isComercial: true,
      isResidencial: true
    };
  }

  handleClickComercial() {
    this.setState({
      isComercial: true,
      isResidencial: false
    });
  }
  handleClickResidencial() {
    this.setState({
      isComercial: false,
      isResidencial: true
    });
  }


  render() {
    return (
      <div className="component__insany">
        <HeaderComponent menuType="goBack" hasLogo={false} hasText text="Criar atendimento" hasSteps steps={2} active={1} />
        <div className="area-cadastro-atendimento">
          <h2>Sobre o Cliente</h2>
          <div className="tipo-cliente-container">
            <TipoCliente onClick={this.handleClickResidencial} active={this.state.isResidencial}>
              <Residencial />
              <h1>RESIDENCIAL</h1>
            </TipoCliente>
            <TipoCliente onClick={this.handleClickComercial} active={this.state.isComercial}>
              <Comercial />
              <h1>COMERCIAL</h1>
            </TipoCliente>
          </div>
          <div className="formulario">
            <div className="inputs">
              <TextInput label="Nome Cliente" type="text" />
              <TextInput label="Endereço" type="text" />
              <TextInput label="Complemento" type="text" />
              <TextInput label="Telefone" type="text" />
            </div>
            <MainButton to="/atendimento/produto" btnText="Próximo" />
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(ClienteAtendimento);
