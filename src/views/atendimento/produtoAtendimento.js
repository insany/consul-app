/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import {withRouter} from 'react-router-dom';

import TextInput from '../../components/textInput';
import HeaderComponent from '../../components/header';
import MainButton from '../../components/button';

import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';


import Select from 'react-select'

import '../../css/atendimento.css'

import produto from '../../img/produto.png';

const useStyles = makeStyles(theme => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    width: '95%',
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2),
  },
}));


const marcas = [
  { value: 'consul', label: 'Consul' },
  { value: '1', label: 'Consul' },
  { value: '2', label: 'Consul' },
]

const tipos = [
  { value: 'consul', label: 'Consul' },
  { value: '1', label: 'Consul' },
  { value: '2', label: 'Consul' },
]

const modelos = [
  { value: 'consul', label: 'Consul' },
  { value: '1', label: 'Consul' },
  { value: '2', label: 'Consul' },
]


const potencias = [
  { value: 'consul', label: 'Consul' },
  { value: '1', label: 'Consul' },
  { value: '2', label: 'Consul' },
]


function SelectInput(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = selectedOption => {
    setOpen(false);
    let elem1 = document.querySelector('.select-input-container').children[0].children[0];
    let elem2 = document.querySelector('.select-input-container').children[0].children[1];

    elem1.setAttribute('data-shrink', true);

    if (elem1.classList.contains('Mui-focused')) {
      elem1.classList.remove('Mui-focused');
    } else {
      elem1.classList.add('Mui-focused');
    }

    if (elem2.classList.contains('Mui-focused')) {
      elem2.classList.remove('Mui-focused');
    } else {
      elem2.classList.add('Mui-focused');
    }

    props.handler(selectedOption.value)
  };

  return (
    <div className="select-container">
      <div className="select-input-container" onClick={handleOpen}>
        <TextInput shrink value={props.value} label={props.label} type="text" />
      </div>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <a className="menu-button menu-opened" onClick={handleClose}>
              <i className="menu-icon "></i>
            </a>
            <Select
              options={props.options}
              onChange={handleChange}
              closeMenuOnSelect
              menuIsOpen
              placeholder="Selecione:"
            />
          </div>

        </Fade>
      </Modal>
    </div>
  );
}

class ProdutoAtendimento extends Component {
    constructor(props) {
    super(props);

    this.MarcaHandler = this.MarcaHandler.bind(this)
    this.TipoHandler = this.TipoHandler.bind(this)
    this.ModeloHandler = this.ModeloHandler.bind(this)
    this.PotenciaHandler = this.PotenciaHandler.bind(this)

    this.state = {
      modal: false,
      marca: null,
      tipo: null,
      modelo: null,
      potencia: null
    };
  }

  MarcaHandler(marcaValue) {
    this.setState({
      marca: marcaValue
    })
  }

  TipoHandler(value) {
    this.setState({
      tipo: value
    })
  }

  ModeloHandler(value) {
    this.setState({
      modelo: value
    })
  }

  PotenciaHandler(value) {
    this.setState({
      potencia: value
    })
  }

  render() {

    return (
      <div className="component__insany">
        <HeaderComponent menuType="goBack" hasLogo={false} hasText text="Criar atendimento" hasSteps steps={2} active={2} />
        <div className="area-cadastro-atendimento">
          <h2>Sobre o produto</h2>
          <div className="img-produto">
            <img src={produto} />
          </div>
          <div className="formulario">
            <div className="inputs">
              <SelectInput label="Marca" value={this.state.marca} options={marcas} handler={this.MarcaHandler} />
              <SelectInput label="Tipo" value={this.state.tipo} options={tipos} handler={this.TipoHandler} />
              <SelectInput label="Modelo" value={this.state.modelo} options={modelos} handler={this.ModeloHandler} />
              <SelectInput label="Potência" value={this.state.potencia} options={potencias} handler={this.PotenciaHandler} />
            </div>
            <MainButton to="/atendimento" from={this.props.location.pathname} btnText="Próximo" />
          </div>
        </div>

      </div>
    )
  }
}

export default withRouter(ProdutoAtendimento);
