import {Link} from 'react-router-dom'
import React, { Component } from 'react'
import styled from 'styled-components'

import sucesso from '../../img/sucesso.svg';
import graduationCap from '../../img/graduation-cap.svg';
import temperature from '../../img/temperature-cold.svg';
import HeaderComponent from './../../components/header';

import Perfil from '../../components/perfil';


const MarcaSelected = styled.div `
  width: 100%;
  height: 31px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.12);
  border-radius: 23px;
  border: 1px solid #b1df50;
  background-color: #b1df50;
  color: #ffffff;
  font-size: 12px;
  font-weight: 500;
  line-height: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
`

export default class ConcluiCadastro extends Component {


  render() {
    return (
      <div className="component__insany">
        <HeaderComponent menuType="menu" hasLogo={false} hasText text="Sucesso!" hasAvatar />

        <div className="area-sucesso area">
          <div className="alerta-sucesso">
            <img src={sucesso} />
            <h2 className="txt-sucesso">Perfil criado <br/> com sucesso!</h2>
          </div>
          <div className="card-cadastro">
            <Perfil />
            <div className="cursos-info s-info">
              <div className="s-title cursos-title">
                <img src={graduationCap} />
                <span className="s-text-title">
                  Cursos e especializações
                </span>
              </div>
              <div className="cursos-info-container">
                <div className="curso-item">
                  <h3 className="curso-info-title">
                    Elétrica avançada p/ instalação de A/C
                  </h3>
                  <div className="curso-info-data">
                    SENAI | 2 anos
                  </div>
                </div>
              </div>
            </div>
            <div className="marcas-info s-info">
              <div className="s-title marcas-title">
                <img src={temperature} />
                <span className="s-text-title">
                  Marcas que atende
                </span>
              </div>
              <div className="marca-info-container">
                <MarcaSelected>
                  Consul
                </MarcaSelected>
                <MarcaSelected>
                  Consul
                </MarcaSelected>
                <MarcaSelected>
                  Consul
                </MarcaSelected>
               <MarcaSelected>
                  Consul
                </MarcaSelected>
                <MarcaSelected>
                  Consul
                </MarcaSelected>
                <MarcaSelected>
                  Consul
                </MarcaSelected>
              </div>
            </div>
          </div>
          <Link to="/" className="btn-sucesso">Concluir</Link>
        </div>

      </div>
    )
  }
}
