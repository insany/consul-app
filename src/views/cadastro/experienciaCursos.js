import React, {Component} from 'react'
import { Link } from 'react-router-dom';

import CursosContainer from './../../components/cursosContainer';
import HeaderComponent from './../../components/header';

import adicionar from '../../img/adicionar.png';

export default class ExperienciaCursos extends Component {
  state = {
    nCursos: 0
  }

  onAddCurso = () => {
    this.setState({
      nCursos: this.state.nCursos + 1
    });
  }



  render(){
    const children = [];

    for (var i = 0; i < this.state.nCursos; i += 1) {
      children.push(<CursosContainer key={i} />);
    };

    const Cursos = props => (
      <div className="cursos">
          {props.children}
      </div>
    );

    return (
      <div>
        <HeaderComponent menuType="goBack" hasLogo={false} hasText text="Experiência" hasSteps steps={3} active={3} />
        <div className="component__insany">
          <div className="area-cadastro area">
            <h2 className="cursos-title">Cursos e especializações</h2>
            <div className="cursos-scroll">
            <div className="formulario" >

                <Cursos addChild={this.onAddCurso}>
                  <CursosContainer />
                  {children}
                </Cursos>

              <div className="btn-add-cursos" onClick={this.onAddCurso}>
                <img src={adicionar} />
                <h3 className="txt-adicionar">ADICIONAR MAIS CURSOS</h3>
              </div>
              <div className="btns">
                <Link to="/cadastro/sucesso" className="btn-proximo"> PROXIMO </Link>
                <Link to="/cadastro/sucesso" className="btn-pular"> Pular Etapa </Link>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
