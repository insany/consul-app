import React from 'react'
import { Link } from 'react-router-dom';
import HeaderComponent from './../../components/header';

import SelectMarca from '../../components/selectMarcas.js';

export default function ExperienciaMarcas() {
  return (
    <div className="component__insany">
      <HeaderComponent menuType="goBack" hasLogo={false} hasText text="Experiência" hasSteps steps={3} active={2} />

      <div className="area-cadastro area">
        <p>Agora conta um poquinho sobre sua experiência como técnico e especializações. Você pode pular esta etapa e preencher depois se preferir.</p>
        <h2 className="form-title">Marcas que atendo</h2>
        <div className="formulario">
          <div className="marcas-container">
              <SelectMarca nomeMarca="Consul" />
              <SelectMarca nomeMarca="Samsung" />
              <SelectMarca nomeMarca="LG" />
              <SelectMarca nomeMarca="Fujitsu" />
              <SelectMarca nomeMarca="Daikin" />
              <SelectMarca nomeMarca="Midea" />
              <SelectMarca nomeMarca="Consul" />
              <SelectMarca nomeMarca="Samsung" />
              <SelectMarca nomeMarca="LG" />
              <SelectMarca nomeMarca="Fujitsu" />
              <SelectMarca nomeMarca="Daikin" />
              <SelectMarca nomeMarca="Midea" />
          </div>
          <Link to="/cadastro/formulario/experiencia/cursos" className="btn-proximo"> PROXIMO </Link>
          <Link to="/cadastro/sucesso" className="btn-pular"> Pular Etapa </Link>
        </div>
      </div>
    </div>
  )
}
