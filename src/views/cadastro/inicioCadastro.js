import React from 'react'
import {Link} from 'react-router-dom'

import '../../css/cadastro.css';

import IconeCalendario from '../../img/icone-calendario.svg';
import IconeOrcamento from '../../img/icone-orcamento.svg';
import IconeCifrao from '../../img/icone-cifrao.svg';
import HeaderComponent from './../../components/header';

export default function InicioCadastro() {
  return (
    <div className='component__insany'>
       <HeaderComponent hasLogo hasAvatar menuType="menu" />
      <div className="tela-inicial">
        <div className="cont">
          <h1>
            Bem-vindo ao
            {' '}
            <span>Ficafrio!</span>
          </h1>
          <p>
              Com o Ficafrio você pode gerenciar o passo-a-passo de seus atendimentos de assistência técnica e instalação. E ainda compartilhar de forma rápida e fácil o que precisar com seus clientes.
          </p>
          <span className="line" />
          <ul>
            <li>
                <div className="icon">
                  <img src={IconeCalendario} />
                </div>
                <span>Gerenciar agendamentos.</span>
              </li>
            <li>
                <div className="icon">
                  <img src={IconeOrcamento} />
                </div>
                <span>Criar orçamentos de forma rápida e integrada.</span>
              </li>
            <li>
                <div className="icon">
                  <img src={IconeCifrao} />
                </div>
                <span>Receber pagamentos na hora.</span>
              </li>
          </ul>
          <Link to="/cadastro/formulario" className='btn-comecar'>Começar</Link>
        </div>
      </div>
    </div>
  )
}
