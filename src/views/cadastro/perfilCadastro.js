/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import TextInput from '../../components/textInput';

import Avatar from '../../img/icone-avatar-lg.svg';
import HeaderComponent from './../../components/header';

class PerfilCadastro extends Component {


  render() {
    return (
      <div className="component__insany">
        <HeaderComponent menuType="goBack" hasLogo={false} hasText text="Crie seu perfil" hasSteps steps={3} active={1} />
        <div className="area-cadastro area">
          <p>Só precisamos de alguns dados para podermos começar. Estes serão os dados que seus clientes terão acesso.</p>
          <div className="formulario">
            <div className="usuario">
              <div className="foto" >
                <input type="file" name="file" onChange={this.onChangeHandler} ></input>
                <img id="imagem-perfil" src={Avatar} />
              </div>
              <span>Adicionar foto</span>
            </div>
            <div className="inputs">
              <TextInput label="Nome Completo" type="text" />
              <TextInput label="Email" type="email"  />
              <TextInput label="Telefone" type="text"  />
            </div>
            <Link to="/cadastro/formulario/experiencia" className="btn-proximo"> PROXIMO </Link>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(PerfilCadastro);
