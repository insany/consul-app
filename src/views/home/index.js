import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import styled from 'styled-components';

import TextInput from '../../components/textInput';
import Avatar from '../../img/icone-avatar-lg.svg';
import HeaderComponent from './../../components/header';
import AtendimentoTabs from './../../components/atendimentoTabs';


import '../../css/home.css'
import MainButton from '../../components/button';

import {ReactComponent as IconTools} from '../../img/icon-tools.svg'
import {ReactComponent as IconStatus} from '../../img/icon-status.svg'
import whatsapp from '../../img/whatsapp.png'

const AtendimentoCard = styled.div`
  cursor: pointer;
  width: 100%;
  height: 72px;
  display: flex;
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  background-color: #ffffff;
  margin-bottom: 18px;

  .info {
    flex-grow: 2;
    display: flex;
    align-items: flex-start;
    justify-content: center;
    flex-direction: column;
    padding: 14px 24px 9px 18px;
    border-right: 1px solid #EAEAEA;

    h2 {
      color: #a1a1a1;
      font-size: 16px;
      font-weight: 400;
      line-height: 20px;
    }

    span {
      color: #a1a1a1;
      font-size: 12px;
      font-weight: 300;
      line-height: 20px;
    }
  }
  .status {
    width: 99px;
    flex-grow: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    .status-icon {

    }

    .status-desc {
      text-align: center;
      max-width: 61px;
      color: #a1a1a1;
      font-size: 8px;
      font-weight: 500;
      line-height: 12px;
      text-transform: uppercase;
    }
  }
`

class AtendimentoCardsSection extends Component {
  atendimentos= this.props.atendimentos;
  render(){

      return(
        this.atendimentos.map((atendimento, i) => {
          if (this.props.tab == 1 && !atendimento.isFinalized) {
            return(
              <AtendimentoCard key={i} >
                <div className="info">
                  <h2>{atendimento.nome}</h2>
                  <span>{atendimento.produto}</span>
                </div>
                <div className="status">
                  <div className="icon-status">
                    <IconStatus />
                  </div>
                  <div className="status-desc">
                    {atendimento.status}
                  </div>
                </div>
              </AtendimentoCard>
            )
          } else if (this.props.tab == 2 && atendimento.isFinalized) {
            return (
              <AtendimentoCard key={i} >
                <div className="info">
                  <h2>{atendimento.nome}</h2>
                  <span>{atendimento.produto}</span>
                </div>
                <div className="status">
                  <div className="icon-status">
                    <img src={whatsapp} />
                  </div>
                  <div className="status-desc">
                    {atendimento.status}
                  </div>
                </div>
              </AtendimentoCard>
            )
          }
        })
      )

  }
}

class PrimeiroAtendimento extends Component {
  render(){
    if(this.props.isFirst || this.props.atendimentos[0] == null){
      return(
        <div className="primeiro-atendimento-container">
          <div className="primeiro-atendimento-icon-container">
            <IconTools />
          </div>
          <h3 className="primeiro-atendimento-titulo">
            Crie seu primeiro atendimento
          </h3>
          <p className="primeiro-atendimento-desc">
            Para começar a criar um projeto tenha em mão as informações de seu cliente e produto.
          </p>
        </div>
      )
    } else {
      return null;
    }
  }
}

class AtendimentoSection extends Component {

  render(){
    if(this.props.tab===1){
      return(
        <div className="atendimentos-container">
          <div className="atendimentos-title">
            <IconTools />
            Meus Atendimentos
          </div>
          <div className="atendimentos-description">
            <span>Projeto</span>
            <span>Status</span>
          </div>
          <div className="area-atendimento">
            {this.props.children}
          </div>
        </div>
      )
    } else if (this.props.tab === 2){
      return(
        <div className="atendimentos-container">
          <div className="atendimentos-title">
            <IconTools />
            Atendimentos Finalizados
          </div>
          <div className="area-atendimento">
            {this.props.children}
          </div>
        </div>
      )
    }
  }
}

class Home extends Component {
      constructor(props) {
        super(props);
        this.state = {
          tab: 1,
          isFirst: false,
          atendimentos: [
            {
              nome: "Projeto Consultório Ana",
              produto: "Consul Split 15.000 BTUs",
              status: "ORÇAMENTO EM APROVAÇÃO",
              isFinalized: false
            },
            {
              nome: "Projeto Lojas Migos",
              produto: "Consul Split Inverter 20.000 BTUs",
              status: "Aguardando Pagamento",
              isFinalized: false
            },
            {
              nome: "Projeto Casa da Carol",
              produto: "Consul Split maxi 9.000 BTUs",
              status: "Entrar em Contato",
              isFinalized: true
            }
          ]
        }
    }

    changeTab(stateValueFromChild) {
        this.setState({tab: stateValueFromChild});
    }


  render() {
    return (
      <div className="component__insany">
        <HeaderComponent menuType="menu" hasLogo hasAvatar />
        <div className="area-home area">
          <AtendimentoTabs handleTabChange={(stateValueFromChild) => this.changeTab(stateValueFromChild)} />
          <AtendimentoSection tab={this.state.tab} >
            <PrimeiroAtendimento isFirst={this.state.isFirst} atendimentos={this.state.atendimentos} />
            <AtendimentoCardsSection atendimentos={this.state.atendimentos} tab={this.state.tab} />
            <MainButton to="/atendimento/criar" btnText="Criar Atendimento" />
          </AtendimentoSection>
        </div>
      </div>
    )
  }
}

export default withRouter(Home);
